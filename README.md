# ZEN PARKING API Service

This is the Api Service for the ZenParking Client Application

## Run: 
  1. Clone this Project.
  2. ``npm install``
  3. nodejs server.js
  
  The Service is now running on Port 8080.
  
## API 

### GET /parkinglots
This will give you an representation of all parkinglots availiable:
```json
{
  "_id": "571255db13937b0b31763833",
  "timestamp": "2016-04-13T19:24:09.336Z",
  "carparks": [
    {
      "label": "C1 Hauptverwaltung MPB, Parkhaus",
      "title": "Parkhaus C1",
      "count": 156,
      "free": 156,
      "location": {
        "lat": 49.4869073,
        "lng": 8.4643374
      },
      "total": 211
    },
    {
      "label": "Collini-Center, Tiefgarage",
      "title": "Tiefgarage Collini Center",
      "count": 284,
      "free": 284,
      "location": {
        "lat": 49.4907435,
        "lng": 8.4760138
      },
      "total": 650
    },
    {
      "label": "Collini-Center Mulde, Parkplatz",
      "title": "Parkplatz Mulde Collini Center",
      "count": 92,
      "free": 92,
      "location": {
        "lat": 49.4910974,
        "lng": 8.4782087
      },
      "total": 213
    },
    {
      "label": "D3, Tiefgarage",
      "title": "Tiefgarage D3",
      "count": 313
    },
    {
      "label": "D5 Reiß-Museum, Tiefgarage",
      "title": "Tiefgarage D5",
      "count": 295,
      "free": 295,
      "location": {
        "lat": 49.488393,
        "lng": 8.462011
      },
      "total": 365
    },
    {
      "label": "G1 Marktplatz, Tiefgarage",
      "title": "Tiefgarage G1",
      "count": 210,
      "free": 210,
      "location": {
        "lat": 49.490057,
        "lng": 8.467282
      },
      "total": 337
    },
    {
      "label": "H6, Tiefgarage",
      "title": "Tiefgarage H6",
      "count": 99,
      "free": 99,
      "location": {
        "lat": 49.492402,
        "lng": 8.4639
      },
      "total": 271
    },
    {
      "label": "Hauptbahnhof P1 , Tiefgarage",
      "title": "Parken Mannheim Hauptbahnhof",
      "count": 152,
      "free": 152,
      "location": {
        "lat": 49.4804565,
        "lng": 8.4705167
      },
      "total": 333
    },
    {
      "label": "Hauptbahnhof P2, Parkhaus",
      "title": "Parken Mannheim Hauptbahnhof",
      "count": 149,
      "free": 149,
      "location": {
        "lat": 49.477775,
        "lng": 8.473606
      },
      "total": 326
    },
    {
      "label": "Hauptbahnhof P3/P4, Parkhaus",
      "title": "Parken Mannheim Hauptbahnhof",
      "count": 84,
      "free": 84,
      "location": {
        "lat": 49.47947,
        "lng": 8.4746705
      },
      "total": 264
    },
    {
      "label": "Klinikum, Tiefgarage",
      "title": "Tiefgarage Klinikum",
      "count": 465,
      "free": 465,
      "location": {
        "lat": 49.4900519,
        "lng": 8.4901095
      },
      "total": 509
    },
    {
      "label": "Klinikum P3, Parkplatz",
      "title": "Parken Mannheim Klinikum",
      "count": 243,
      "free": 243,
      "location": {
        "lat": 49.490138,
        "lng": 8.489996
      },
      "total": 313
    },
    {
      "label": "M4a, Parkplatz",
      "title": "Parkplatz M4a",
      "count": 32,
      "free": 32,
      "location": {
        "lat": 49.4844,
        "lng": 8.466775
      },
      "total": 68
    },
    {
      "label": "N1/N2 Stadthaus, Parkhaus",
      "title": "Parkhaus N1 und N2",
      "count": 474,
      "free": 474,
      "location": {
        "lat": 49.485842,
        "lng": 8.464592
      },
      "total": 727
    },
    {
      "label": "N6 Komfort, Parkhaus",
      "title": "Parkhaus N6",
      "count": 164,
      "free": 164,
      "location": {
        "lat": 49.4847931,
        "lng": 8.4702103
      },
      "total": 201
    },
    {
      "label": "N6 Standard, Parkhaus",
      "title": "Parkhaus N6",
      "count": 234,
      "free": 234,
      "location": {
        "lat": 49.485001,
        "lng": 8.469333
      },
      "total": 323
    },
    {
      "label": "Nationaltheater, Parkplatz",
      "title": null,
      "count": 90,
      "free": 90,
      "location": {
        "lat": 49.488632,
        "lng": 8.479102
      },
      "total": 160
    },
    {
      "label": "U2, Tiefgarage",
      "title": "Tiefgarage U2",
      "count": 119
    },
    {
      "label": "Wasserturm, Tiefgarage",
      "title": null,
      "count": 340,
      "free": 340,
      "location": {
        "lat": 49.4845647,
        "lng": 8.4760609
      },
      "total": 406
    },
    {
      "label": "SAP Arena P1, Parkplatz",
      "title": "Parken an der SAP Arena",
      "count": 344,
      "free": 344,
      "location": {
        "lat": 49.460984,
        "long": 8.516357
      },
      "total": 348
    },
    {
      "label": "SAP Arena P2, Parkhaus",
      "title": "Parken an der SAP Arena",
      "count": 0,
      "free": 0,
      "location": {
        "lat": 49.462821,
        "lng": 8.516004
      },
      "total": 815
    },
    {
      "label": "SAP Arena P3, Parkplatz",
      "title": "Parken an der SAP Arena",
      "count": 0,
      "free": 0,
      "location": {
        "lat": 49.465133,
        "lng": 8.522301
      },
      "total": 850
    },
    {
      "label": "Messe P6-P8, Parkplatz",
      "title": "Parken an der SAP Arena",
      "count": 1500,
      "free": 1500,
      "location": {
        "lat": 49.468215,
        "lng": 8.521819
      },
      "total": 1520
    }
  ]
}
```
### GET /forecast
This will give you the forecast of all weekdays: 
```json
{
  "C1 Hauptverwaltung MPB, Parkhaus": [
    {
      "weekday": 0,
      "values":[0...47],
      "totalSlots": 211,
    }, {...}
    ],
}
```
### GET /wisedom
Give you a string with some Open Data Facts.
```json
"2014 wurden ca. 2.500 Schüler in die Grundschule eingeschult."
```