var express    = require('express');
var app        = express();
var mongodb    = require('mongodb');
var moment     = require('moment');

var MongoClient = mongodb.MongoClient;

var port = process.env.PORT || 8080;

app.get('/parkinglots', function (req, res) {

    console.log(typeof req.query.timestamp);

    if(typeof req.query.timestamp != 'undefined') {
        var timestamp = new Date(parseInt(req.query.timestamp));
    } else {
        var timestamp = null;
    }

    var url = 'mongodb://localhost:27017/carpark_status_hackathon';
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            var collection = db.collection('carparks_timestamps');

            var lowRange = moment(timestamp).subtract(10, 'minutes');
            var topRange = moment(timestamp).subtract(6, 'minutes');

            console.log(timestamp);
            if(timestamp != null) {
                var restrict = {
                    timestamp: {
                        $gte: new Date(lowRange.format()),
                        $lt: new Date(topRange.format())
                    }
                };
            } else {
                restrict = {};
            }

            items = collection.findOne(restrict, function (err, result) {
                db.close();

                if (err) { console.log(err) }

                if(result == null) {
                    result = []
                }
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Credentials', true);
                res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
                res.header('Access-Control-Allow-Headers', 'Content-Type');
                res.end(JSON.stringify(result));
            });
        }
    });
});

var carparksTotalSlots = {};

app.get('/forecast', function (req, res) {

    var url = 'mongodb://localhost:27017/carpark_status_hackathon';
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {

            var collection = db.collection('carparks_timestamps');
            items = collection.find().toArray(function (err, result) {
                db.close();
                console.log("Found "+result.length+" entries.");
                var aggregation = [];

                //Iterate through data
                for (i = 0; i < result.length; i++) {
                    var date = result[i].timestamp;
                    var timeslot = getTimeSlot(date);
                    var weekday = (new Date(date)).getDay();
                    if (!aggregation[weekday]) {
                        aggregation[weekday] = [];
                    }
                    if (!aggregation[weekday][timeslot]) {
                        aggregation[weekday][timeslot] = [];
                    }

                    //Iterate through carparks
                    result[i].carparks.forEach(function (carpark) {
                        if (!aggregation[weekday][timeslot][carpark.label]) {
                            aggregation[weekday][timeslot][carpark.label] = [];
                        }
                        var free = carpark.free || carpark.count;
                        aggregation[weekday][timeslot][carpark.label].push(free);
                        carparksTotalSlots[carpark.label] = carpark.total;
                    });
                }

                aggregationJson = parseWeekAgregationToJson(
                    calculateAverageValues(aggregation)
                );

                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Credentials', true);
                res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
                res.header('Access-Control-Allow-Headers', 'Content-Type');
                res.end(aggregationJson);

            });
        }
    });
});


app.get('/wisedom', function (req, res) {
    var wisedom_of_the_day = [
        "Im Jahr 2014 wurden 1.482 Jungen in Mannheim geboren. Die Namen Alexander und Maximilian waren bei den Eltern besonders beliebt.",
        "Im Jahr 2014 wurden 1.404 Mädchen in Mannheim geboren. Die Namen Sophie und Marie waren bei den Eltern besonders beliebt",
        "In der Neckarstadt/Ost wurden im Jahr 2014 340 Paare Eltern!",
        "2014 wurden ca. 2.500 Schüler in die Grundschule eingeschult.",
        "2015 gab es in Mannheim besonders viele Neugeborene mit dem Namen Elias",
        "2015 gab es in Mannheim besonders viele Neugeborene mit dem Namen Sophie",
        "Knapp 40% der Römisch-katholischen Mannheimer Bürger haben einen Migrationshintergrund.",
        "11% der evangelischen Mannheimer Bürger haben einen Migrationshintergrund",
        "46% der Mannheimer gehören keiner öffentl. rechtlichen Religionsgemeinschaft an.",
        "In Mannheim sind 137 Altkleidercontainer aufgestellt!",
        "Im ganzen Stadtgebiet gibt es 314 Altglascontainer!",
        "Im Jahr 2014 sind die Mannheimer fast 600.000 mal ins Schwimmbad gegangen.",
        "In 2011 waren die Mannheimer besoders faul - nur 531.557 Besuche in Schwimmbädern wurden gezählt."
    ];

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.end(JSON.stringify(wisedom_of_the_day[Math.floor(Math.random() * wisedom_of_the_day.length)]));


});


function getTimeSlot(date) {
    return date.getHours() * 2 + Math.floor(date.getMinutes() / 30);
}

function calculateAverageValues(aggregation) {
    aggregation.forEach(function (weekday) {
        weekday.forEach(function (timeslot) {
            for(var key in timeslot){
                var carpark = timeslot[key];
                var n = carpark.length;
                var sum = carpark.reduce(function (a, b) {
                    return a + b;
                });
                if(n > 0) {
                    timeslot[key] = sum / n;
                }
                else {
                    timeslot[key] = 0;
                }
            }
        });
    });
    return aggregation;
}

function parseWeekAgregationToJson(aggregation){
    var target = {};
    aggregation.forEach(function (weekday, weekday_index) {
        var weekday = weekday;
        weekday.forEach(function (timeslot, timeslot_index) {
            for(var key in timeslot){
                var carparkName = key;
                var free = timeslot[key];
                if(!target[carparkName]) {
                    // Create the Carparks in the object.
                    target[carparkName] = [];
                }
                var found = false;
                target[carparkName].forEach(function (dayObject,index,array) {
                    if(dayObject.weekday === weekday_index) {
                        found = true;
                        var totalSlots = carparksTotalSlots[carparkName];
                        var percent = (free*100) / totalSlots;
                        array[index].values.push(percent);
                    }
                });
                if(!found) {
                    var dayObject = {};
                    dayObject.weekday = weekday_index;
                    dayObject.values = [];
                    var totalSlots = carparksTotalSlots[carparkName];
                    dayObject.totalSlots = totalSlots; 
                    var percent = (free*100) / totalSlots;
                    dayObject.values.push(percent);
                    target[carparkName].push(dayObject);
                }
            }
        });
    });
    return JSON.stringify(target);
}


app.listen(8080);
console.log('Magic happens on port ' + port);
